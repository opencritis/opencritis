# opencritis

** WIP - This Project Is Work In Progress**

Open Critical Information Infrastructures Security

This project aims to provide a secure and trusted runtime for devices; providing a secure linux system and test-framework.

This project has been inspired by the [BSI ICS-Kompendium](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/ICS/ICS-Security_kompendium_pdf.pdf?__blob=publicationFile)

## License

This project is unsing the Apache License 2.0  http://www.apache.org/licenses/

## Pre-Requisites
* docker buildroot SDK
* git
* qemu

## Concepts

This system shall use a verifed boot path.
The root file system shall be safe against manipulation using the kernel feature 'verity'.
SystemD shall be used as init process, managing resources and networking.
Seamless A/B system updates shall be supported. Rauc shall be used to realize A/B system update bundles.

## Building

OpenCritis' primary target is `qemu_arm_vexpress_tz` permitting automated tests. Ports to imx6  and stm32mp1 are planned for the future.
 
Get the project from gitlab page [OpenCritis](https://gitlab.com/opencritis/opencritis)
```
git clone git@gitlab.com:opencritis/opencritis.git
```

A default buildroot SDK docker container is used to build OpenCritis. The bulidprocess will open a shell in docker buildroot docker container and starting the build process.

```
$ cd opencritis
$ TARGET=qemu_arm_vexpress_tz make shell-build 
```
The following attributes can be defined in build environment

| Environment Variable   | Description                                                                              |
| ---------------------- | ---------------------------------------------------------------------------------------- |
| OC_BOOT_DELAY          | 3 seconds by default, if set to -2 the uboot start process cannot be interrupted anymore |
| OC_ROOT_PASSWORD       | 'abcd1234' by default, should be set for release builds, will be used by tests as well.  |
| OC_KEYTOOL_URL         | URL where the board specific keytool can be fetched from, either file://... or http://... |  
| OC_SECRETS_URL         | URL where the secrets file can be fetched from, either file://... or http://... | 
| OC_SECRETS_PASS_FILE   | passphrase file being read in prepare-scripts and sign-scripts to unlock private keys or perform login to pkcs11 interfaces | 
 
After build, the images will be located in directory buildroot/output/qemu_arm_vexpress_tz/images/ and can be started  within bulidroot SDK shell calling
```
$ cd opencritis
$ make shell
br-user@358660c94807:/builds$ TARGET=qemu_arm_vexpress_tz make start-sys
```

## Secret Keys
Secret keys for each boot phase are expected in folder `./ext-opencritis/secrets/`, namely `rootfs-keys/`, `kernel-keys/` and `bootloader-keys/`. If the keys do not exist, the build process will generate new ones. Each board will require a specific definition of OC_KEYTOOL_URL. 

The keys can be generated manually calling 
```
$ make secrets.tar
```
For release bulids, a single file `secrets.tar" should  be archived and used for future release builds. 
```
$ TARGET=qemu_arm_vexpress_tz \
  OC_KEYTOOL_URL=file://tools/keytool.zip \
  OC_SECRETS_URL=file://$PWD/secrets.tar \
  OC_SECRETS_PASS_FILE=/secure/passphrase.txt \
  OC_BOOT_DELAY=-2 \
  make secrets-restore shell-build-test
```

## Automated Testing
Automated tests are based on python unittest and pexpect. Tests are located in sub directories
./tests/ and ./tests-boot/. The tests are executed within the docker shell of the buildroot SDK

```
$ cd opencritis
$ make shell
br-user@358660c94807:/builds$ TARGET=qemu_arm_vexpress_tz make tests
br-user@358660c94807:/builds$ TARGET=qemu_arm_vexpress_tz make tests-boot
```
The test report will be available in folder `./junit-reports/`.

All these tests are executed for each commit in gitlab [pipeline](https://gitlab.com/opencritis/opencritis/-/pipelines)

All tests of  `./tests-boot/` are starting a fresh session, booting the board, wheras all tests in `./tests/` share a single session.

Each test must perform a login at first into a fresh shell, and logout at last.

 
## Spec and Sizing

### Sizing

The minimal size of the root filesysem is 35MB, mostly being occupied by systemD binaries and rauc binaries. The disk image is occupying about 500MB. 

### Partitions

| Partition   | Format    | Size    | Description  |
| ----------- | ----------| ------- | ------------ |
| (fsbl)      |           |         | trusted ARM  firmware | 
| (ssbl)      |           |         | Uboot binary |
| (env)       | uboot-env | 32KB    | optional Uboot environment |     
| boot        | fat       | 64MB    | plain partition kernel images, uboot scripts, and uboot-environment,  |
| roota       | ext2      | >=35MB  | readonly rootfs partition  |
| hasha       |           | ca 4MB  | verity hash tree of partition roota   |
| rootb       | ext2      | >=35MB  | readonly rootfs partition  |
| hashb       |           | ca 4MB  | verity hash tree of partition rootb   |
| efs         | LUKS/ext4 | >=64MB  | encrypted files system, read-write partition |
| journal     | LUKS/ext4 | >=64MB  | encrypted journal/log partition. Buffering logs before upload, bridging reboots.  |    
| pack        | LUKS/ext4 | >=128MB | encrypted read-write partition storing download signed packages. The signature must be verified before unpacking! |
 
Note: Any LUKS partitions must be at least of size 32MB for the LUKS header.

#### Partition Labels
Due to verity, and updating identical image to partition roota or rootb their ext2-labels are identical, must not be changed. Otherwise the verity hash tree would become invalid.
 
Fact is, labels are written directly into fat-partitions and ext2-partitions. This is ugly, instead use the labels from GPT partition table; using sym-links in `/dev/disk/by-partlabel/` instead of  `/dev/disk/by-label/`. These `partLabel` attribues are stored in GPT parition table in disk-header!

### Executables

Executables must be binaries or SH-Scripts. Beside SH-shell, no other interpreters are shipped.  Executables can be created using C/C++ or RustC during build process.

## Credits

### General
1. http://trac.gateworks.com/wiki/secure_boot
1. https://www.timesys.com/security/dm-verity-without-an-initramfs/
1. swTPM: https://github.com/stefanberger/swtpm
1. https://wiki.emacinc.com/wiki/Loading_Images_with_U-Boot
1. https://wiki.emacinc.com/wiki/Loading_Images_onto_eMMC_Devices
1. https://github.com/jaydcarlson/u-boot-imx6ul
1. https://imxdev.gitlab.io/tutorial/How_to_boot_imx_using_ramdisk/
1. https://www.linuxfromscratch.org/blfs/view/basic/initramfs.html
1. https://community.nxp.com/t5/i-MX-Processors/Boot-with-initramfs/m-p/735499

### STM32
1. https://wiki.st.com/stm32mpu/wiki/Getting_started/STM32MP1_boards/STM32MP157x-DK2/Develop_on_Arm%C2%AE_Cortex%C2%AE-A7/Modify,_rebuild_and_reload_the_Linux%C2%AE_kernel


