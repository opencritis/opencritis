
.PHONY: build rebuild docker-create docker-shell build menuconfig linux-rebuild linux-menuconfig  tests tests-boot tests-all secrets secrets-tar setup shell-build shell-tests-all

# may be pre-defined as environment variable
TARGET ?= qemu_arm_vexpress_tz

ifeq ($(TARGET),stm32mp157c_dk2)
    MANUFACTURER = stmicroelectronics
endif
ifeq ($(TARGET),qemu_arm_vexpress_tz)
    MANUFACTURER = qemu
endif


BUILDROOT_TAG=2022.08.x
BUILDROOT_REPO="https://github.com/buildroot/buildroot.git"
BUILDROOT_DIR=buildroot
BUILDROOT_PATH=$(abspath $(BUILDROOT_DIR))
EXTERNAL_OPENCRITICS=$(abspath ./ext-opencritis)
SECRETS_DIR=$(EXTERNAL_OPENCRITICS)/secrets

CONTAINER=buildroot/base:20210922.2200

OUTPUT=output
OUTPUT_OPENCRITIS=$(OUTPUT)/$(TARGET)

CONFIG_FILE="$(TARGET)_opencritis_defconfig"

## NOTE: the variable OC_ROOT_PASSWORD shall be defined in build environment!!
OC_ROOT_PASSWORD ?= "\"abcd1234\""
OC_BOOT_DELAY ?= "3"
OC_SECRETS_URL ?= 
OC_KEYTOOL_URL ?= 

# load target specific variables
include $(EXTERNAL_OPENCRITICS)/board/$(MANUFACTURER)/$(TARGET)/custom.mk

guard-%:
	@ if [ "${${*}}" = "" ]; then \
	    echo "Environment variable $* not set"; \
        exit 1; \
      fi
    
$(BUILDROOT_DIR):
	git clone --branch=$(BUILDROOT_TAG) $(BUILDROOT_REPO)
	
docker-pull: $(BUILDROOT_DIR)
	if ! docker ps -a | grep $(CONTAINER) 2> /dev/null; then docker pull -q $(CONTAINER); fi

shell: docker-pull
	docker run -v "$(PWD)":/builds/ -w /builds -t -i $(CONTAINER) 
		
shell-build: $(BUILDROOT_DIR) docker-pull 
	docker run -v "$(PWD)":/builds/ -w /builds -t -i $(CONTAINER) make TARGET=$(TARGET) build
	
shell-tests-all: $(BUILDROOT_DIR) docker-pull
	docker run -v "$(PWD)":/builds/ -w /builds -t -i $(CONTAINER) make TARGET=$(TARGET) tests-all
	
# 
# BUILD OPENCRITIS
#

# stm32qsmp:
# 	AARCH=$(ARCH)  make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) stm32mp157c_dk2_defconfig
# 	AARCH=$(ARCH)  make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) 
# 
# stm32qsmp-savedefconfig:
# 	AARCH=$(ARCH)  make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) savedefconfig
# 
# stm32qsmp-linux-savedefconfig:
# 	AARCH=$(ARCH)  make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) linux-savedefconfig


$(SECRETS_DIR)/rootfs-keys/cert.pem $(SECRETS_DIR)/rootfs-keys/public.pem: 
	mkdir -p $(SECRETS_DIR)/rootfs-keys
	$(EXTERNAL_OPENCRITICS)/scripts/prepare-rootfs-keys.sh  $(SECRETS_DIR)/rootfs-keys
	
$(SECRETS_DIR)/kernel-keys/dev.crt: 
	mkdir -p $(SECRETS_DIR)/kernel-keys
	openssl genrsa -F4 -out $(SECRETS_DIR)/kernel-keys/dev.key 2048
	openssl req -batch -new -x509 -key $(SECRETS_DIR)/kernel-keys/dev.key -out $(SECRETS_DIR)/kernel-keys/dev.crt


secrets: $(SECRETS_DIR)/rootfs-keys/public.pem  $(SECRETS_DIR)/kernel-keys/dev.crt
	
secrets.tar: $(SECRETS_DIR)/rootfs-keys/public.pem 
	tar -C $(EXTERNAL_OPENCRITICS) -cf secrets.tar secrets
	@echo "Note: Archive the file ./secrets.tar"
	@echo "      For future release builds restore the secrets"
	@echo "$ TARGET=.... OC_KEYTOOL_URL=... OC_SECRETS_URL=file:///storage/secrets.tar make secrets-restore shell-build"

secrets-clean:
	rm -rf $(SECRETS_DIR) secrets.tar
	
secrets-restore: guard-OC_SECRETS_URL
	curl --output - $(OC_SECRETS_URL)  | tar -C $(EXTERNAL_OPENCRITICS) -xf -

setup:
	$(EXTERNAL_OPENCRITICS)/scripts/config-set.sh  $(EXTERNAL_OPENCRITICS)/configs/$(CONFIG_FILE) \
	      BR2_TARGET_GENERIC_ROOT_PASSWD "$(OC_ROOT_PASSWORD)"
	$(EXTERNAL_OPENCRITICS)/scripts/config-set.sh  $(EXTERNAL_OPENCRITICS)/board/$(MANUFACTURER)/$(TARGET)/uboot.env \
	      bootdelay "$(OC_BOOT_DELAY)"

$(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS): setup $(BUILDROOT_DIR)
	AARCH=$(ARCH)  make -C $(BUILDROOT_DIR) BR2_EXTERNAL=$(EXTERNAL_OPENCRITICS) O=$(OUTPUT_OPENCRITIS) $(CONFIG_FILE)

sdk: $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) sdk
	
uboot: $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) uboot
	
uboot-reconfigure: $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) uboot-reconfigure

uboot-rebuild: $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) uboot-rebuild

uboot-update-defconfig: $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) uboot-update-defconfig

uboot-menuconfig: $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) uboot-menuconfig
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) uboot-savedefconfig
	UBOOT_VERSION=`grep "^BR2_TARGET_UBOOT_CUSTOM_VERSION_VALUE="  $(EXTERNAL_OPENCRITICS)/configs/$(CONFIG_FILE)  | cut -d '"' -f 2`; \
	cp $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)/build/uboot-$${UBOOT_VERSION}/defconfig $(EXTERNAL_OPENCRITICS)/board/$(MANUFACTURER)/$(TARGET)/uboot.config
	
linux: $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) linux
	
linux-reconfigure: $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) linux-reconfigure

linux-menuconfig: $(EXT_TOOLCHAIN_ARCH_DIR)  $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS) $(RAUC_KEY)
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) linux-menuconfig
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) linux-savedefconfig
	cp $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)/build/linux-5*/defconfig $(EXTERNAL_OPENCRITICS)/board/$(MANUFACTURER)/$(TARGET)/linux.config
	
menuconfig: $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) menuconfig
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) savedefconfig
	
	
linux-rebuild: $(EXT_TOOLCHAIN_ARCH_DIR)  $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS) $(RAUC_KEY)
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) linux-rebuild


# build toolchain (incl tool xxd) then build the rest, otherwise xxd would be built after uboot 
# as it does not have any dependency to it
$(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)/images/sdcard.img: $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)
	ARCH=$(ARCH) make -j8 -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) toolchain host-xxd all

~/.local:
	cd 3rdparty; \
	rm -rf unittest-xml-reporting-master; \
	unzip xml-runner-*.zip; \
	cd unittest-xml-reporting-master; \
	python3 setup.py install --user
	
tests: ~/.local
	python3 -m xmlrunner discover -v -s ./tests/ -o ./junit-reports

tests-boot: ~/.local
	python3 -m xmlrunner discover -v -s ./tests-boot/ -o ./junit-reports

tests-all: tests tests-boot
	
build: $(SECRETS_DIR)/rootfs-keys/public.pem $(SECRETS_DIR)/rootfs-keys/cert.pem  $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)/images/sdcard.img
	
rebuild: 
	rm -f $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)/images/sdcard.img
	$(MAKE) build

start-sys:
	cp -a $(EXTERNAL_OPENCRITICS)/board/$(MANUFACTURER)/$(TARGET)/start-sys.sh $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)/images/start-sys.sh
	$(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)/images/start-sys.sh
	
clean:
	ARCH=$(ARCH) make -C $(BUILDROOT_DIR) O=$(OUTPUT_OPENCRITIS) clean
	
veryclean:
	rm -rf $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS) 
	
sdcard: guard-DEV $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)/images/sdcard.img 
	@echo "Copying $(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)/images/sdcard.img to $(DEV).."
	while ! test -b $(DEV) -a -b $(DEV)1; do echo -n "." ; sleep 0.5; done
	for partition in $(DEV)*; do echo "unmounting $$partition"; sudo umount -f "$$partition" || true; done
	sudo dd if=$(BUILDROOT_DIR)/$(OUTPUT_OPENCRITIS)/images/sdcard.img of=$(DEV) status=progress bs=512 conv=notrunc conv=sync
	sync; sudo eject $(DEV)
	

sdcard-gen: guard-DEV guard-IMG
	@echo "Copying $(IMG) to $(DEV).."
	for partition in $(DEV)*; do echo "unmounting $$partition"; sudo umount -f "$$partition" || true; done
	sudo dd if=$(IMG) of=$(DEV) status=progress bs=512 conv=sync conv=notrunc
	sync; sudo eject $(DEV)
