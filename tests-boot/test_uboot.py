import unittest

from pylib import opencritis
from pylib import config
import pexpect

@unittest.skipIf(config.env_val("OC_BOOT_DELAY", "3") <= 0, "requires OC_BOOT_DELAY > 0")
class Test(unittest.TestCase):
    
    def setUp(self):
        self.session = opencritis.Session();
    	        
    def test_env_bootcmd_static(self):
        INDEX_OF_MANIPULTED = 0
    
        self.session.child.expect('Hit any key to stop autoboot:', timeout=config.COMMAND_SECS)
        self.session.child.sendline('\n')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('printenv bootcmd')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('setenv bootcmd manipulated')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('saveenv')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('printenv bootcmd')
        self.session.child.expect('bootcmd=manipulated', timeout=config.COMMAND_SECS)
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.powercycle()

        self.session.child.expect('Hit any key to stop autoboot:', timeout=config.COMMAND_SECS)
        self.session.child.sendline('\n')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
                        
        self.session.child.sendline('env load')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('printenv bootcmd')
        self.assertTrue(INDEX_OF_MANIPULTED != self.session.child.expect(['bootcmd=manipulated', pexpect.TIMEOUT ], timeout=config.COMMAND_SECS))

    def test_env_boot_order_write(self):
        INDEX_OF_MANIPULTED = 0
    
        self.session.child.expect('Hit any key to stop autoboot:', timeout=config.COMMAND_SECS)
        self.session.child.sendline('\n')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
         
        self.session.child.sendline('setenv boot_order AB')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('saveenv')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('printenv boot_order')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.expect('boot_order=AB', timeout=config.COMMAND_SECS)
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.powercycle()

        self.session.child.expect('Hit any key to stop autoboot:', timeout=config.COMMAND_SECS)
        self.session.child.sendline('\n')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
                        
        self.session.child.sendline('env load')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('printenv boot_order')
        self.session.child.expect('boot_order=AB', timeout=config.COMMAND_SECS)

    def test_env_boot_a_left_write(self):
        INDEX_OF_MANIPULTED = 0
    
        self.session.child.expect('Hit any key to stop autoboot:', timeout=config.COMMAND_SECS)
        self.session.child.sendline('\n')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
         
        self.session.child.sendline('setenv boot_a_left 1') 
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('saveenv')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)

        self.session.child.sendline('printenv boot_a_left')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)

        self.session.child.expect('boot_a_left=1', timeout=config.COMMAND_SECS)
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.powercycle()

        self.session.child.expect('Hit any key to stop autoboot:', timeout=config.COMMAND_SECS)
        self.session.child.sendline('\n')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
                        
        self.session.child.sendline('env load')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('printenv boot_a_left')
        self.session.child.expect('boot_a_left=1', timeout=config.COMMAND_SECS)

    def test_env_boot_b_left_write(self):
        INDEX_OF_MANIPULTED = 0
    
        self.session.child.expect('Hit any key to stop autoboot:', timeout=config.COMMAND_SECS)
        self.session.child.sendline('\n')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
         
        self.session.child.sendline('setenv boot_b_left 1')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('saveenv')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('printenv boot_b_left')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.expect('boot_b_left=1', timeout=2)
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.powercycle()

        self.session.child.expect('Hit any key to stop autoboot:', timeout=config.COMMAND_SECS)
        self.session.child.sendline('\n')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
                        
        self.session.child.sendline('env load')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('printenv boot_b_left')
        self.session.child.expect('boot_b_left=1', timeout=config.COMMAND_SECS)

    def test_env_malicious_ignore(self):
        INDEX_OF_MANIPULTED = 0
    
        self.session.child.expect('Hit any key to stop autoboot:', timeout=config.COMMAND_SECS)
        self.session.child.sendline('\n')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
         
        self.session.child.sendline('setenv malicious 0')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('saveenv')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('printenv malicious')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.expect('malicious=0', timeout=config.COMMAND_SECS)
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.powercycle()

        self.session.child.expect('Hit any key to stop autoboot:', timeout=config.COMMAND_SECS)
        self.session.child.sendline('\n')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
                        
        self.session.child.sendline('env load')
        self.session.child.expect('=>', timeout=config.COMMAND_SECS)
        
        self.session.child.sendline('printenv malicious')
        self.assertTrue(INDEX_OF_MANIPULTED != self.session.child.expect(['malicious=0', pexpect.TIMEOUT ], timeout=config.COMMAND_SECS))
                                
    def tearDown(self):
        self.session.close()
