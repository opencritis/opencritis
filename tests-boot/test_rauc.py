import unittest

from pylib import opencritis
from pylib import config

class Test(unittest.TestCase):
    
    def setUp(self):
        self.session = opencritis.Session();
    	        
    def test_rauc_slot_A_to_B(self):
        self.session.login(timeout=config.STARTUP_SECS)
        self.session.call_succeeds("cat /proc/cmdline | grep 'rauc.slot=A'")
        self.session.call_succeeds("fw_printenv boot_order | grep 'AB'")
        self.session.call_succeeds("fw_setenv boot_order BA && sync")
        self.session.powercycle()
        self.session.login(timeout=config.STARTUP_SECS)
        self.session.call_succeeds("cat /proc/cmdline | grep 'rauc.slot=B'")
        self.session.call_succeeds("fw_printenv boot_order | grep 'BA'")
        self.session.logout()
    
    def tearDown(self):
        self.session.close()
