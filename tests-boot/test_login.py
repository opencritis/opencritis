import unittest

from pylib import opencritis
from pylib import config

class Test(unittest.TestCase):
    
    def setUp(self):
        self.session = opencritis.Session();
    	        
    def test_powercycle_login(self):
        self.session.login(timeout=config.STARTUP_SECS)
        self.session.logout()
        self.session.powercycle()
        self.session.login(timeout=config.STARTUP_SECS)
        self.session.logout()
    
    def tearDown(self):
        self.session.close()
