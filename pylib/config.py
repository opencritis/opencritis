import os

STARTUP_SECS=60
COMMAND_SECS=5
    
def user():
    # TODO, read form env
    return "root"
    
def passwd():
    # TODO, read form env
    return "abcd1234"
    
def hostname():
    # TODO, read form env
    return "opencritis"
    
def qemu_starter():
    # TODO, read form env
    return "./buildroot/output/qemu_arm_vexpress_tz/images/start-sys.sh"
    
def env_val(name, default):
    return int(os.getenv(name, default))
