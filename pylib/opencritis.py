import pexpect
import unittest
import subprocess
import time
from contextlib import suppress

from pylib import config 


class Session:
    def __init__(self, starter=None):
        self.starter = config.qemu_starter() if starter is None else starter 
        subprocess.run([self.starter, 'session-setup'])
        self.child = self.__spawn_child()
        
    def  __spawn_child(self):
        child = pexpect.spawn(self.starter, ['session'])    	
        child.delaybeforesend = 0.5
        child.delayafterclose = 0.5
        child.delayafterterminate = 0.5
        return child
      
    def __terminate_child(self):
        # may fail if child terminates too fast
        with suppress(pexpect.exceptions.ExceptionPexpect): self.child.close()
        self.child.wait()
        # release resources before newly spawn
        self.child = None
        
    def login(self, timeout=config.COMMAND_SECS):
        hostname = config.hostname()
        user = config.user()
        passwd = config.passwd()
        self.child.expect(['login:.*', pexpect.TIMEOUT], timeout=timeout) 
        self.child.sendline(user)
        self.child.expect('Password:.*', timeout=config.COMMAND_SECS)
        self.child.sendline(passwd)
        self.child.expect('0\|root# ')
        
        
    def logout(self, timeout=config.COMMAND_SECS):
        for repeat in [1,2,3]:
       	    self.child.sendcontrol("d")
            index = self.child.expect(['Welcome.*', pexpect.TIMEOUT], timeout=timeout)
            if index == 0:
                return ;

    def force_logout(self):
        self.logout(timeout=config.COMMAND_SECS)

    def call_succeeds(self, cmd, timeout=config.COMMAND_SECS):
        self.child.sendline(cmd)
        self.child.expect('0\|root# ', timeout=timeout)
        
    def call_fails(self, cmd, timeout=config.COMMAND_SECS):
        self.child.sendline(cmd)
        self.child.expect('1\|root# ', timeout=timeout)
   
    def powercycle(self):
        self.__terminate_child()
        self.child = self.__spawn_child()
        
    def close(self):
        self.__terminate_child()
        subprocess.run([self.starter, 'session-teardown'])
        
	
_shared_session = None
_ref_count = 0

class TestCaseShared(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        unittest.TestCase.__init__(self, *args, **kwargs)
        
        global _shared_session
        global _ref_count
        if not _shared_session:
            _shared_session = Session();
        _ref_count = _ref_count + 1     

    def session(self):
        return _shared_session
        
    def __del__(self):
        global _shared_session
        global _ref_count
        _ref_count = _ref_count - 1;
        if _ref_count == 0:
            _shared_session.close()
            _shared_session = None
            
