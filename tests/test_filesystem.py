import unittest
import time

from pylib import opencritis
from pylib import config

class TestFilesystem(opencritis.TestCaseShared):
    def setUp(self):
        self.session().login(timeout=config.STARTUP_SECS)

    def tearDown(self):
        self.session().logout()

    def test_readonly_rootfs(self):
        self.session().call_succeeds("mount | grep -E ' / .*\(ro' ")
    
    def test_run(self):
        self.session().call_succeeds("test -e /run")

    def test_var_run(self):
        self.session().call_succeeds("test -e /var/run")
        
    def test_var_lock(self):
        self.session().call_succeeds("test -e /var/lock")
        
    def test_run_lock(self):
        self.session().call_succeeds("test -e /run/lock")        
       
    def test_boot_zImage(self):
        self.session().call_succeeds("test -e /boot/image-a.ub")
        self.session().call_succeeds("test -e /boot/image-b.ub")
        
    def test_etc_fw_env_config(self):
        self.session().call_succeeds("test -e /etc/fw_env.config")
        
    def test_fw_printenv(self):
        self.session().call_succeeds("fw_printenv")   
        
    def test_true(self):
        self.session().call_succeeds('true')   
        
    def test_false(self):
        self.session().call_fails('false')   

    def test_foobar_fails(self):
        self.session().call_fails('test -e /foobar')   
