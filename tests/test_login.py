import unittest

from pylib import opencritis
from pylib import config

class TestLogin(opencritis.TestCaseShared):
    def test_login(self):
        self.session().login(timeout=config.STARTUP_SECS)
        self.session().logout()
        
    def test_logout_login(self):
        self.session().login(timeout=config.STARTUP_SECS)
        self.session().logout()
        self.session().login()
        self.session().logout()
