# Verity Setup

Intention is to use a transparent verity device of the rootfs. 
The verity device shall be initialized directly without usage of initrd-

## References
* https://www.timesys.com/security/dm-verity-without-an-initramfs/?unapproved=2412&moderation-hash=2f87e96457999a01289451b3202aa05b#comment-2412
* https://wiki.archlinux.org/title/Dm-verity
  
## Kernel Modules
The following kernel modules must be activated (static) in linux.config. The module `CONFIG_DM_INIT=y` permits the direct initialization. The certificate `verity_cert.pem` is generated in a later step and is used to validate the verity root hash signature. 

```
CONFIG_BLK_DEV_DM=y
CONFIG_DM_CRYPT=y
CONFIG_DM_UEVENT=y
CONFIG_DM_VERITY=y
CONFIG_DM_VERITY_FEC=y

CONFIG_DM_INIT=y
CONFIG_DM_VERITY_VERIFY_ROOTHASH_SIG=y
CONFIG_SYSTEM_TRUSTED_KEYRING=y
CONFIG_SYSTEM_TRUSTED_KEYS="verity_cert.pem"
```
The following kernel features must be acticated if the dmsetup tool shall be used on the target in userspace

```
CONFIG_CRYPTO_USER_API_HASH=y
CONFIG_CRYPTO_USER_API_SKCIPHER=y
CONFIG_CRYPTO_USER_API_RNG=y
```
## Kernel Patch
Unfortunately, DM_VERITY_VERIFY_ROOTHASH_SIG does not support early mapping, as it uses the kernel keyring system and does not provide a way to setup the keyring via the kernel command-line arguments.

Alright, fine, let’s fix it. We’ll add a new verity table parameter called “root_hash_sig_hex”, where we can set the incoming root hash signature, without the keyring.

```
From: Nathan Barrett-Morrison <nathan.morrison@timesys.com>
Date: Wed, 30 Mar 2022 10:57:25 -0400
Subject: [PATCH 1/1] DM-Verity: Add root_hash_sig_hex parameter to early
 verity device mapping, so that we can pass a roothash signature in via
 /proc/cmdline.  This enables the ability to use DM_VERITY_VERIFY_ROOTHASH_SIG
 alongside early device mapping
```
```diff
diff --git a/drivers/md/dm-verity-target.c b/drivers/md/dm-verity-target.c
index d3e76ae..7d267d6 100644
--- a/drivers/md/dm-verity-target.c
+++ b/drivers/md/dm-verity-target.c
@@ -972,7 +972,13 @@ static int verity_parse_opt_args(struct dm_arg_set *as, struct dm_verity *v,
 			if (r)
 				return r;
 			continue;
-
+                } else if (verity_verify_is_hex_sig_opt_arg(arg_name)) {
+                        r = verity_verify_hex_sig_parse_opt_args(as, v,
+                                 verify_args,
+                                 &argc, arg_name);
+                        if (r)
+                               return r;
+                        continue;
 		}
 
 		ti->error = "Unrecognized verity feature request";
diff --git a/drivers/md/dm-verity-verify-sig.c b/drivers/md/dm-verity-verify-sig.c
index db61a1f..7010909 100644
--- a/drivers/md/dm-verity-verify-sig.c
+++ b/drivers/md/dm-verity-verify-sig.c
@@ -28,6 +28,12 @@ bool verity_verify_is_sig_opt_arg(const char *arg_name)
 			    DM_VERITY_ROOT_HASH_VERIFICATION_OPT_SIG_KEY));
 }
 
+bool verity_verify_is_hex_sig_opt_arg(const char *arg_name)
+{
+    return (!strcasecmp(arg_name,
+                DM_VERITY_ROOT_HASH_VERIFICATION_OPT_HEX_SIG_KEY));
+}
+
 static int verity_verify_get_sig_from_key(const char *key_desc,
 					struct dm_verity_sig_opts *sig_opts)
 {
@@ -64,6 +70,33 @@ static int verity_verify_get_sig_from_key(const char *key_desc,
 	return ret;
 }
 
+static int verity_verify_get_sig_from_hex(const char *key_desc,
+                    struct dm_verity_sig_opts *sig_opts)
+{
+    int ret = 0, i = 0, j = 0;
+    uint8_t byte[3] = {0x00, 0x00, 0x00};
+    long result;
+
+    sig_opts->sig = kmalloc(strlen(key_desc)/2, GFP_KERNEL);
+    if (!sig_opts->sig) {
+        ret = -ENOMEM;
+        goto end;
+    }
+
+    sig_opts->sig_size = strlen(key_desc)/2;
+
+    for(i = 0, j = 0; i < strlen(key_desc)-1; i+=2, j+=1){
+        byte[0] = key_desc[i];
+        byte[1] = key_desc[i+1];
+        kstrtol(byte, 16, &result);
+        sig_opts->sig[j] = result;
+    }
+
+end:
+
+    return ret;
+}
+
 int verity_verify_sig_parse_opt_args(struct dm_arg_set *as,
 				     struct dm_verity *v,
 				     struct dm_verity_sig_opts *sig_opts,
@@ -93,6 +126,36 @@ int verity_verify_sig_parse_opt_args(struct dm_arg_set *as,
 	return ret;
 }
 
+int verity_verify_hex_sig_parse_opt_args(struct dm_arg_set *as,
+                     struct dm_verity *v,
+                     struct dm_verity_sig_opts *sig_opts,
+                     unsigned int *argc,
+                     const char *arg_name)
+{
+    struct dm_target *ti = v->ti;
+    int ret = 0;
+    const char *sig_key = NULL;
+
+    if (!*argc) {
+        ti->error = DM_VERITY_VERIFY_ERR("Signature key not specified");
+        return -EINVAL;
+    }
+
+    sig_key = dm_shift_arg(as);
+    (*argc)--;
+
+    ret = verity_verify_get_sig_from_hex(sig_key, sig_opts);
+    if (ret < 0)
+        ti->error = DM_VERITY_VERIFY_ERR("Invalid key specified");
+
+    v->signature_key_desc = kstrdup(sig_key, GFP_KERNEL);
+    if (!v->signature_key_desc)
+        return -ENOMEM;
+
+    return ret;
+}
+
+
 /*
  * verify_verify_roothash - Verify the root hash of the verity hash device
  *			     using builtin trusted keys.
diff --git a/drivers/md/dm-verity-verify-sig.h b/drivers/md/dm-verity-verify-sig.h
index 3987c71..4779ae5 100644
--- a/drivers/md/dm-verity-verify-sig.h
+++ b/drivers/md/dm-verity-verify-sig.h
@@ -10,6 +10,7 @@
 
 #define DM_VERITY_ROOT_HASH_VERIFICATION "DM Verity Sig Verification"
 #define DM_VERITY_ROOT_HASH_VERIFICATION_OPT_SIG_KEY "root_hash_sig_key_desc"
+#define DM_VERITY_ROOT_HASH_VERIFICATION_OPT_HEX_SIG_KEY "root_hash_sig_hex"
 
 struct dm_verity_sig_opts {
 	unsigned int sig_size;
@@ -24,6 +25,13 @@ int verity_verify_root_hash(const void *data, size_t data_len,
 			    const void *sig_data, size_t sig_len);
 bool verity_verify_is_sig_opt_arg(const char *arg_name);
 
+bool verity_verify_is_hex_sig_opt_arg(const char *arg_name)
+{
+    return false;
+}
+
+bool verity_verify_is_hex_sig_opt_arg(const char *arg_name);
+
 int verity_verify_sig_parse_opt_args(struct dm_arg_set *as, struct dm_verity *v,
 				    struct dm_verity_sig_opts *sig_opts,
 				    unsigned int *argc, const char *arg_name);
@@ -52,6 +60,13 @@ static inline int verity_verify_sig_parse_opt_args(struct dm_arg_set *as,
 	return -EINVAL;
 }
 
+static inline int verity_verify_hex_sig_parse_opt_args(struct dm_arg_set *as, struct dm_verity *v,
+                    struct dm_verity_sig_opts *sig_opts,
+                    unsigned int *argc, const char *arg_name)
+{
+    return -EINVAL;
+}
+
 static inline void verity_verify_sig_opts_cleanup(struct dm_verity_sig_opts *sig_opts)
 {
 }
```
## Buildroot tools

In Buildroot the following host tools must be activated
```
tune2fs
cryptsetup/dmsetup
```

Optionally, activate the following target packages
```
tune2fs
cryptsetup/dmsetup
```
## GPT disk layout

Beside ssbl and fsbl bootloader partitions, create at least the first two volumes.

1. ...
1. Root (formatted as ext4, f2fs, erofs LABEL=Root)
1. Verity (LABEL=Verity)
1. Home (optional if you want write access for a user)
1. Var (many programs will not run if /var is not writable)

/home and /var should be writeable filesystems. On a server that just has one purpose this may be optional, since e.g. a wireguard server needs no write access to the disk. 

## Setting up Root Hash Signing Key

Signature verification in the Linux kernel is performed with `CONFIG_DM_VERITY_VERIFY_ROOTHASH_SIG=y`. This is a kernel configuration option which was added in 5.4. It adds the ability to do roothash signature verification in Linux’s kernel-space without any external tools or libraries.

This is utilized by creating an RSA certificate from the build system:

```
PRIVATE_KEY="verity_key.pem"
CERT="verity_cert.pem"
openssl req -x509 -newkey rsa:1024 -keyout ${PRIVATE_KEY} \
    -out ${CERT} -nodes -days 1825 -set_serial 01 -subj /CN=example.com
```

## Setting up verity

1. Create the root file system
1. Generate the verity image (format)
1. Sign the verity root hash
1. Define boot parameters initializing the verity block device /dev/dm-0 and refer as root=/dev/dm-0
1. Build a disk image containing the root file system, verity image and boot loader set up

### Generate Verity image

Lets process the hash tree for the ext2/ext4 data partition`DATA_DEV=images/rootfs.ext2`, and `HASH_DEV=images/rootfs.hash`

One may initialize the HASH_DEV with specific size, for example `HASH_DEV_SIZE=16M`. 
```
truncate -s${HASH_DEV_SIZE} ${HASH_DEV} 
```

The required size is related to the block-count and block size DATA_DEV.

The DATA_BLOCK_SIZE of the ext2/ext4 DATA_DEV may be read the following way. Buildroot may use a block size of 1024 instead of 4096.

```
DATA_BLOCK_SIZE=$(tune2fs -l ${DATA_DEV} | grep  "Block size:"  | cut -d":" -f 2| xargs)
```

Use the following command to create the verity hash tree in HASH_DEV
```
veritysetup format --data-block-size=${DATA_BLOCK_SIZE} ${DATA_DEV} ${HASH_DEV} 
```
The output may look like
```
VERITY header information for images/rootfs.hash
UUID:            	dabfa8bf-fa6d-4e2e-b1d0-dd3e9c92dbaa
Hash type:       	1
Data blocks:     	122880
Data block size: 	1024
Hash block size: 	4096
Hash algorithm:  	sha256
Salt:            	8bf77965478f88fdd66a54e9b30adbc7c1f6bb60b8fa0d424c5460cec8f2812d
Root hash:      	0cb14c83f560742ce14e5371226925510f1f993459ce7bfd0d16e9028798afca
```
The following command extracts the relevant parameters, so that these may be stored for a shell script
```
veritysetup format --data-block-size=${DATA_BLOCK_SIZE} ${DATA_DEV} ${HASH_DEV} | sed -n -e 's/Hash type:\s*/HASH_TYPE=/p'  -e 's/Data blocks:\s*/DATA_BLOCKS=/p' -e 's/Hash block size:\s*/HASH_BLOCK_SIZE=/p' -e 's/Data block size:\s*/DATA_BLOCK_SIZE=/p' -e 's/Hash algorithm:\s*/HASH_ALGORITHM=/p' -e 's/Salt:\s*/SALT=/p' -e 's/Root hash:\s*/ROOT_HASH=/p'
```
printing
```
HASH_TYPE=1
DATA_BLOCKS=122880
DATA_BLOCK_SIZE=1024
HASH_BLOCK_SIZE=4096
HASH_ALGORITHM=sha256
SALT=8bf77965478f88fdd66a54e9b30adbc7c1f6bb60b8fa0d424c5460cec8f2812d
ROOT_HASH=0cb14c83f560742ce14e5371226925510f1f993459ce7bfd0d16e9028798afca
```
### Signing the Root Hash

```
ROOTHASH="b96a69664f9279857931dbf64f942caf909076e40fd5bd5ed8d30b53ff922941"
PRIVATE_KEY="verity_key.pem"
CERT="verity_cert.pem"
echo ${ROOTHASH} | tr -d '\n' > roothash.txt
openssl smime -sign -nocerts -noattr -binary -in roothash.txt \
    -inkey ${PRIVATE_KEY} -signer ${CERT} -outform der -out roothash.txt.signed
```

## Uboot Environment 
```
setenv DATA_BLOCKS 16384
setenv DATA_BLOCK_SIZE 4096
setenv DATA_SECTORS 131072
setenv HASH_BLOCK_SIZE 4096
setenv HASH_ALG sha256
setenv SALT 2a4c7638f03b92bdb92d7284a742e0c4407c9ef65fdf2a7ea78ed02fde4a518b
setenv ROOT_HASH b96a69664f9279857931dbf64f942caf909076e40fd5bd5ed8d30b53ff922941
setenv DATA_DEV mmcblk0p1
setenv DATA_META_DEV mmcblk0p2
setenv VERITY_SIGNATURE 3081f906092a864886f70d010702a081eb3081e8020101310f300d06096086480165030402010500300
    b06092a864886f70d0107013181c43081c1020101301b30163114301206035504030c0b6578616d706c652e636f6d020101300d
    06096086480165030402010500300d06092a864886f70d010101050004818071fcaaf1b252a56448438e0a9350b7380a407b1e9
    0ae869ec5062466b0eb6cc5358e253a9d57086c358220745bc60c2a6d8dbc30c02fb1714c9c98f10e0679b87deb0c19929675c8
    fcf89f37c684f043583fca52729ffb6e928eb29b7ee0c9eab3a3b0809a4463f3c8d6d458745c9116a7df1677c707df6352f2323
    13a62ce20

setenv bootargs ${bootargs} dm-mod.create="verity,,,ro,0 \${DATA_SECTORS} verity 1 /dev/\${DATA_DEV}
    /dev/\${DATA_META_DEV} \${DATA_BLOCK_SIZE}  \${HASH_BLOCK_SIZE}  \${DATA_BLOCKS} 1 \${HASH_ALG}
    \${ROOT_HASH} \${SALT} 3 ignore_zero_blocks root_hash_sig_hex \${VERITY_SIGNATURE}"
    root=/dev/dm-0 dm_verity.require_signatures=1
```

## Configuring the kernel command line

Add the following options to your kernel command line:

* `lsm=lockdown`
* `lockdown=confidentiality` optional but helps keep kernel and userspace seperate
* `ro` to prevent changes to root if not using erofs
*  `systemd.verity=1`
*    roothash=contents_of_roothash.txt
*    systemd.verity_root_options=restart-on-corruption or panic-on-corruption (the default behavior will just print an error to dmesg and will not prevent untrusted code from running)
*    systemd.verity_root_data=/dev/OS/Root
*    systemd.verity_root_hash=/dev/OS/Verity
*    rd.emergency=reboot to prevents access to a shell if the root is corrupt
*    rd.shell=0 to prevents access to a shell if boot fails

Um eine Verity-Partition zu booten, ohne InitRd, habe ich folgendes Script erstellt (Anhang). Mit diesemwird  eine Hash-Partition erstellt und es wird die  entsprechende Verity-Table für die Uboot-Kernel-bootargs ausgegebenen wird.

Aktuell erstelle ich die verity-Hash-Partition auf dem Device selbst:

# DATA_DEV=/dev/mmcblk0p4
# HASH_DEV=/dev/mmcblk0p5
# /opt/bin/verity-gen.sh $DATA_DEV $HASH_DEV

Die Ausgabe des Scripts sind die  bootargs Parameter in der Art:

dm-mod.create="system,,,ro,0 245760 verity 1 /dev/mmcblk0p4 /dev/mmcblk0p5 1024 4096 122880 1 sha256 0ee122fa6549e4290eb1873befc77514ea3ac880746b9d4647c5aa1247c149ef 680eb423fc1bac3e6f2a48ad2b7f23cce751f8a3aa4161c707f7e31d7baf9d5d 1 ignore_zero_blocks"

Für das Folgende muss /etc/fstab folgendermaßen aussehen

# cat /etc/fstab
/dev/dm-0 / auto ro 0 1

Nun wird die Verity-Table per bootargs Parameter an den Kernel übergeben, und es ergeben sich folgende bootargs in Uboot, beachte  root=/dev/dm-0


----uboot-----

ext4load mmc 0:4 ${loadaddr} /boot/zImage
ext4load mmc 0:4 ${fdt_addr_r} /boot/stm32mp157c.dtb

setenv bootargs earlyprintk console=ttySTM0,115200 ro rootwait root=/dev/dm-0 'dm-mod.create="system,,,ro,0 245760 verity 1 /dev/mmcblk0p4 /dev/mmcblk0p5 1024 4096 122880 1 sha256 0ee122fa6549e4290eb1873befc77514ea3ac880746b9d4647c5aa1247c149ef 680eb423fc1bac3e6f2a48ad2b7f23cce751f8a3aa4161c707f7e31d7baf9d5d 1 ignore_zero_blocks"'

bootz ${loadaddr} - ${fdt_addr_r}

----/uboot-----

Der Kernel wird nun ein dm-Device /dev/dm-0 eirichten, das durch die Hashwerte des HASH-DEV auf Integrität geshützt sind.

Der Kernel und Systemd werden /dev/dm-0 als RootFS mounten (ro)

Um das "Secure-Boot" zu relaisieren  und die Integrität des System zu sichern, müssen das U-Boot-Binary, der Kernel, der DTB das UBoot-Script (mit den Root-hashwerten) durch die STM32-Signatur-Prüfung validiert werden.

Damit der Hash-Root authentisch ist, kann dieser zustätzlich per Public-Key Verfashren signiert werden und vom Kernerl verifziert werden.

In diesem Fall müsste der Public-Key in den Kernel eingebaut werden und bootarts um folgenden Parameter ergänzt werden

dm_verity.require_signatures=1


Das Script (Anhang) beherscht noch nicht das Signieren das Root-Hash.


Im Kernel müssen folgende Feature gesetzt werden (static)

CONFIG_CRYPTO_USER_API_HASH=y
CONFIG_CRYPTO_USER_API_SKCIPHER=y
CONFIG_CRYPTO_USER_API_RNG=y

CONFIG_BLK_DEV_DM=y
CONFIG_DM_CRYPT=y
CONFIG_DM_UEVENT=y
CONFIG_DM_VERITY=y
# important!!!
CONFIG_DM_INIT=y
CONFIG_DM_VERITY_VERIFY_ROOTHASH_SIG=y
CONFIG_DM_VERITY_FEC=y

# Kernel Feaure für die Signature-Validierung des Root-hash

CONFIG_DM_VERITY_VERIFY_ROOTHASH_SIG=y
CONFIG_SYSTEM_TRUSTED_KEYRING=y
CONFIG_SYSTEM_TRUSTED_KEYS="verity_cert.pem"


Um das Script (Anhang) auszuführen, müssen in der Umgebung (Anhang) folgende Binaries vorliegen:
veritysetup
tune2fs
openssl # zukünftig zum Signieren des Root-Hash

Anbei ein Screenshot, dass der Bootvorgang mit diesem Setup funktioniert und das Device /dev/dm-0 als Root genutzt wird.

Mit diesem Setup würde eine "Modifikation der Software" im Rootfs erkannt und das System würde den Bootvorgang unterbreichen. Der Watchdog würde in das Recovery-System A/B wechseln können.
