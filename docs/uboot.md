
# U-Boot 

U-Boot is used as SSBL located in partition ssbl.

The ssbl partition contains the U-Boot binary and stores two environment settings (redudant)

One can configure the offsets of these environments in uboot.config, but uboot binary is calculating its own offset addresses, moving the environment sectiosn to the end of the partition.

The followng offset addresses have been reverse-engineered. Choosing an 4MB ssbl partition and CONFIG_ENV_SECT_SIZE=0x40000, the uboot binary will choose the following addresses:
 
CONFIG_ENV_OFFSET=0x480400
CONFIG_ENV_OFFSET_REDUND=0x482400

For each rootfs root_a and root_b a verity hash tree will be calculated.

The hash trees will be stored in partitions root_a_hash and root_b_hash and the corresponding root-hash values will be signed and stored in these environment sections

The environments will be generated when creating the sdcard image, and each time the rootfs partitions are updated.

