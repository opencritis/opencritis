#!/bin/bash -e

IMAGE="$1"
HASH="$2"
SECRETS_DIR="${3:-}"

SECTOR_SIZE=512

DEV="${DEV:-${IMAGE}}"
HASH_DEV="${HASH_DEV:-${HASH}}"

block_metrics() {
  tune2fs -l "$IMAGE" | while read line; do 
  case "$line" in
    Block\ size:*)  echo -n "${line#Block\ size:}" 
     ;;
    Block\ count:*) echo -n "${line#Block\ count:}"
     ;;
  esac
  done
}

veritysetup_format() {
  LOC_BLOCK_SIZE="$1"
  LOC_IMAGE="$2"
  LOC_HASH="$3"

  veritysetup format --data-block-size="$LOC_BLOCK_SIZE" "$LOC_IMAGE" "$LOC_HASH" | while read line; do 
  case "$line" in
    UUID:*) echo -n "${line#UUID:}"
      ;; 
    Hash\ type:*) echo -n "${line#Hash type:}"
      ;;
    Data\ blocks:*) echo -n "${line#Data blocks:}"
      ;;
    Data\ block\ size:*) echo -n "${line#Data block size:}"
      ;;
    Hash\ block\ size:*) echo -n "${line#Hash block size:}"
      ;;
    Hash\ algorithm:*) echo -n "${line#Hash algorithm:}"
      ;;
    Salt:*) echo -n "${line#Salt:}"
      ;;
    Root\ hash:*)  echo -n "${line#Root hash:}"
      ;;	
  esac
  done 
}

main() {
  LOC_IMAGE="$1"
  LOC_HASH="$2"
  
  BLOCK_METRICS=$( block_metrics "$LOC_IMAGE" | xargs )
  DATA_BLOCK_COUNT=$(echo "$BLOCK_METRICS" | cut -d ' ' -f 1)
  DATA_BLOCK_SIZE=$(echo "$BLOCK_METRICS"  | cut -d ' ' -f 2)
  DATA_SECTORS=$(( ${DATA_BLOCK_COUNT} * ${DATA_BLOCK_SIZE} / ${SECTOR_SIZE} ))
  
  FORMAT_METRICS=$(veritysetup_format "$DATA_BLOCK_SIZE" "$LOC_IMAGE" "$LOC_HASH" | xargs) 
  
  HASH_TYPE=$(echo "$FORMAT_METRICS" | cut -d ' ' -f 2 )
  DATA_BLOCKS=$(echo "$FORMAT_METRICS" | cut -d ' ' -f 3 )
  HASH_BLOCK_SIZE=$(echo "$FORMAT_METRICS" | cut -d ' ' -f 5 )
  HASH_ALG=$(echo "$FORMAT_METRICS" | cut -d ' ' -f 6 )
  SALT=$(echo "$FORMAT_METRICS" | cut -d ' ' -f 7 )
  ROOT_HASH=$(echo "$FORMAT_METRICS" | cut -d ' ' -f 8 )
 
  NUM_TABLES=1
  NUM_OPTS=1


  echo "setenv DATA_SECTORS ${DATA_SECTORS}"
  echo "setenv DATA_BLOCKS ${DATA_BLOCKS}"
  echo "setenv DATA_BLOCK_SIZE ${DATA_BLOCK_SIZE}"
  echo "setenv HASH_BLOCK_SIZE ${HASH_BLOCK_SIZE}"
  echo "setenv HASH_TYPE ${HASH_TYPE}"
  echo "setenv HASH_ALG ${HASH_ALG}"
  echo "setenv ROOT_HASH ${ROOT_HASH}"
  echo "setenv SALT ${SALT}"
 
 
  if [ "$SECRETS_DIR"  ]; then
      ROOT_HASH_FILE="/tmp/roothash.txt"
      ROOT_HASH_SIGFILE="/tmp/roothash.txt.signed"
      
      echo "${ROOT_HASH}" | tr -d '\n' > "$ROOT_HASH_FILE"
      ${BR2_EXTERNAL_OPENCRITIS_PATH}/scripts/sign-rootfs-roothash.sh "$SECRETS_DIR" "${ROOT_HASH_FILE}" "${ROOT_HASH_SIGFILE}"
      VERITY_SIGNATURE=$(xxd -p "${ROOT_HASH_SIGFILE}" | tr -d '\n')
      echo "setenv VERITY_SIGNATURE ${VERITY_SIGNATURE}"
      # TODO(fr):  rm -f "${ROOT_HASH_SIGFILE}" "$ROOT_HASH_FILE"
  fi  
  

# these values shall in the following kernel parameter  
#  echo "dm-mod.create=\"system,,,ro,0 ${DATA_SECTORS} verity ${NUM_TABLES} ${DEV} ${HASH_DEV} ${DATA_BLOCK_SIZE} ${HASH_BLOCK_SIZE} ${DATA_BLOCKS} ${HASH_TYPE} ${HASH_ALG} ${ROOT_HASH} ${SALT} ${NUM_OPTS} ignore_zero_blocks\"" 
    
}

main "$IMAGE" "$HASH"

