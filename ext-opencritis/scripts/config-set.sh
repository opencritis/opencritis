#!/bin/sh

VARFILE="$1"
VARNAME="$2"
VALUE="$3"

# modify if not set yet
if ! grep "${VARNAME}=${VALUE}" ${VARFILE}; then
  sed -i "/^${VARNAME}=/{h;s/=.*/=${VALUE}/};\${x;/^$/{s//${VARNAME}=${VALUE}/;H};x}" ${VARFILE}
fi
