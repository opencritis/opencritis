#!/bin/bash -e

KEYS_DIR="${1}"
DOMAIN="${2:-opencritis.org}"
DAYS="${3:-3650}"

KEY_BITS="1024"

PRIVATE_KEY="${KEYS_DIR}/private.pem"
PUBLIC_KEY="${KEYS_DIR}/public.pem"
CERT="${KEYS_DIR}/cert.pem"


# openssl genpkey -algorithm RSA -out ${PRIVATE_KEY} -pkeyopt rsa_keygen_bits:4096
# openssl rsa -pubout -in ${PRIVATE_KEY} -out ${PUBLIC_KEY}

openssl req -x509 -newkey ec:<(openssl ecparam -name secp384r1) -keyout ${PRIVATE_KEY} \
    -out ${CERT} -nodes -days "${DAYS}" -set_serial 01 -subj "/CN=${DOMAIN}"
    
openssl ec -pubout -in ${PRIVATE_KEY} -out ${PUBLIC_KEY}
