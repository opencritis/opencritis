#!/bin/bash -e

# may make use of environment of variables 
#   OC_SECRETS_PASS_FILE: to unlock private keys or perform pkcs11 access

SECRETS_DIR="$1"
ROOT_HASH_FILE="$2"
ROOT_HASH_SIGFILE="$3"

PRIVATE_KEY="$SECRETS_DIR/private.pem"
CERT="$SECRETS_DIR/cert.pem"

openssl smime -sign -nocerts -noattr -binary -in "${ROOT_HASH_FILE}" \
          -inkey ${PRIVATE_KEY} -signer ${CERT} -outform der -out "${ROOT_HASH_SIGFILE}"
