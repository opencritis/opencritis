
################################################################################
#
# XXD
#
################################################################################

# source included in buildroot, taken from
# https://github.com/ckormanyos/xxd
HOST_XXD_LICENSE = Public Domain

define HOST_XXD_EXTRACT_CMDS
        cp $(HOST_XXD_PKGDIR)/*.c  $(@D)
endef

define HOST_XXD_BUILD_CMDS
        $(HOSTCC) $(HOST_CFLAGS) $(HOST_LDFLAGS) \
                $(@D)/xxd.c  \
                -o $(@D)/xxd
endef

define HOST_XXD_INSTALL_CMDS
        $(INSTALL) -D -m 755 $(@D)/xxd $(HOST_DIR)/bin/xxd
endef

$(eval $(host-generic-package))

