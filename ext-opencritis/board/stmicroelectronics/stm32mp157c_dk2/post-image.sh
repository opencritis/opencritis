#!/usr/bin/env bash 

set -x
set -e


#
# atf_image extracts the ATF binary image from DTB_FILE_NAME that appears in
# BR2_TARGET_ARM_TRUSTED_FIRMWARE_ADDITIONAL_VARIABLES in ${BR_CONFIG},
# then prints the corresponding file name for the genimage
# configuration file
#
atf_image()
{
	local ATF_VARIABLES="$(sed -n 's/^BR2_TARGET_ARM_TRUSTED_FIRMWARE_ADDITIONAL_VARIABLES="\([\/a-zA-Z0-9_=. \-]*\)"$/\1/p' ${BR2_CONFIG})"

	if grep -Eq "DTB_FILE_NAME=stm32mp157c-dk2.dtb" <<< ${ATF_VARIABLES}; then
		echo "tf-a-stm32mp157c-dk2.stm32"
	elif grep -Eq "DTB_FILE_NAME=stm32mp157a-dk1.dtb" <<< ${ATF_VARIABLES}; then
                echo "tf-a-stm32mp157a-dk1.stm32"
	elif grep -Eq "DTB_FILE_NAME=stm32mp157a-avenger96.dtb" <<< ${ATF_VARIABLES}; then
                echo "tf-a-stm32mp157a-avenger96.stm32"
	fi
}

verity_image() {
        VERITY_PARAM_FILE="$1"
        ROOTFS_IMAGE="$2"
        VERITY_IMAGE="$3"
        VERITY_PARAM_LABEL="$4"
        
        ${BR2_EXTERNAL_OPENCRITIS_PATH}/scripts/verity-gen.sh "${ROOTFS_IMAGE}" "${VERITY_IMAGE}" "${VERITY_PARAM_LABEL}"> ${VERITY_PARAM_FILE}
}

uboot_script() {
        SCRIPT_FILE="$1"
        FILE1="$2"
        FILE2="$3"
        FILE3="$4"
        SCRIPT_TXT_FILE="$(mktemp)"
        cat "$FILE1" "$FILE2" "$FILE3" > "$SCRIPT_TXT_FILE"
        
        cat "$SCRIPT_TXT_FILE"
        
        mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n "boot script" -d "$SCRIPT_TXT_FILE" "$SCRIPT_FILE"
}

main()
{
	local ATFBIN="$(atf_image)"
	local GENIMAGE_CFG="$(mktemp --suffix genimage.cfg)"
	local GENIMAGE_TMP="${BUILD_DIR}/genimage.tmp"
	local FILES='"stm32mp157c-dk2.dtb", "zImage", "u-boot.scr.uimg"'
          
        verity_image ${BINARIES_DIR}/rootfsa.txt ${BINARIES_DIR}/rootfs.ext2 ${BINARIES_DIR}/rootfsa.hash rootfsa_  
        
	verity_image  ${BINARIES_DIR}/rootfsb.txt ${BINARIES_DIR}/rootfs.ext2 ${BINARIES_DIR}/rootfsb.hash rootfsb_ 
	
	uboot_script ${BINARIES_DIR}/u-boot.scr.uimg ${BINARIES_DIR}/rootfsa.txt ${BINARIES_DIR}/rootfsb.txt ${BR2_EXTERNAL_OPENCRITIS_PATH}/board/stmicroelectronics/stm32mp157c_dk2/u-boot.scr 

	sed  -e "s/%FILES%/${FILES}/" \
	     -e "s/%ATFBIN%/${ATFBIN}/" \
		${BR2_EXTERNAL_OPENCRITIS_PATH}/board/stmicroelectronics/stm32mp157c_dk2/genimage.cfg.template > ${GENIMAGE_CFG}
		
	cat ${GENIMAGE_CFG}

	support/scripts/genimage.sh -c ${GENIMAGE_CFG}

	rm -f ${GENIMAGE_CFG}

	exit $?
}

main $@
