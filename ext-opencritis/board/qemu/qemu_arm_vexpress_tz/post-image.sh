#!/usr/bin/env bash 

set -e


verity_image() {
        VERITY_PARAM_FILE="$1"
        ROOTFS_IMAGE="$2"
        VERITY_IMAGE="$3"
        ROOTFS_SECRETS_DIR="$4"
        ${BR2_EXTERNAL_OPENCRITIS_PATH}/scripts/verity-gen.sh "${ROOTFS_IMAGE}" "${VERITY_IMAGE}" "${ROOTFS_SECRETS_DIR}"  > ${VERITY_PARAM_FILE}
}

uboot_script() {
        SCRIPT_FILE="$1"
        DATA_DEV="$2"
        DATA_META_DEV="$3"
        RAUC_SLOT="$4"
        FILE1="$5"
        FILE2="$6"
        
        cat <<EOF > "$SCRIPT_FILE"
DATA_DEV=${DATA_DEV}
DATA_META_DEV=${DATA_META_DEV}
RAUC_SLOT=${RAUC_SLOT} 
EOF
        cat "$FILE1" "$FILE2"  >> "$SCRIPT_FILE"
        
        cat "$SCRIPT_FILE"
}

main()
{
	local GENIMAGE_CFG="$(mktemp --suffix genimage.cfg)"
	local GENIMAGE_TMP="${BUILD_DIR}/genimage.tmp"
	local FILES='"image-a.ub", "image-b.ub"'

        set -x
        
        #
        #
        # 
        verity_image ${BINARIES_DIR}/rootfs.txt ${BINARIES_DIR}/rootfs.ext2 ${BINARIES_DIR}/rootfs.hash ${BR2_EXTERNAL_OPENCRITIS_PATH}/secrets/rootfs-keys/ 
        
	uboot_script ${BINARIES_DIR}/u-boot-slot-a.scr \
	          /dev/vda2 /dev/vda3  "A"             \
	          ${BINARIES_DIR}/rootfs.txt           \
	          ${BR2_EXTERNAL_OPENCRITIS_PATH}/board/qemu/qemu_arm_vexpress_tz/u-boot.scr.in

	uboot_script ${BINARIES_DIR}/u-boot-slot-b.scr \
	          /dev/vda4 /dev/vda5  "B"             \
	          ${BINARIES_DIR}/rootfs.txt           \
	          ${BR2_EXTERNAL_OPENCRITIS_PATH}/board/qemu/qemu_arm_vexpress_tz/u-boot.scr.in
	
	## create DTS
	cp ${BR2_EXTERNAL_OPENCRITIS_PATH}/board/qemu/qemu_arm_vexpress_tz/armvirt.dts.in  ${BINARIES_DIR}/armvirt.dts
	output/qemu_arm_vexpress_tz/host/bin/dtc -I dts -O dtb -o ${BINARIES_DIR}/armvirt.dtb ${BINARIES_DIR}/armvirt.dts
		
	## crate fit image and sign with kernel key
	cp ${BR2_EXTERNAL_OPENCRITIS_PATH}/board/qemu/qemu_arm_vexpress_tz/fit.its ${BINARIES_DIR}/fit.its

	( cd ${BINARIES_DIR} ; 
	  mkimage -f fit.its image-a.ub ;
	  mkimage -F image-a.ub -k ${BR2_EXTERNAL_OPENCRITIS_PATH}/secrets/kernel-keys/ -K armvirt.dtb -c "sjg@varts-storage.com 2023" -r
	  cp image-a.ub image-b.ub 
	)
	
	sed  -e "s/%FILES%/${FILES}/" \
		${BR2_EXTERNAL_OPENCRITIS_PATH}/board/qemu/qemu_arm_vexpress_tz/genimage.cfg.template > ${GENIMAGE_CFG}
		
	cat ${GENIMAGE_CFG}

	support/scripts/genimage.sh -c ${GENIMAGE_CFG}

	rm -f ${GENIMAGE_CFG}

        #
        #
        #
        cp ${BR2_EXTERNAL_OPENCRITIS_PATH}/board/qemu/qemu_arm_vexpress_tz/start-sys.sh ${BINARIES_DIR}/
        
	exit $?
}

main $@
