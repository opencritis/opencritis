#!/bin/sh

set -u
set -e

# Create flash.bin fromu-boot.bin and sufficient space for env
cd "$BINARIES_DIR"
dd if=u-boot.bin of=flash.bin bs=4096
# dd if=fip.bin of=flash.bin seek=64 bs=4096 conv=notrunc
truncate -s 64M flash.bin

