#!/bin/sh
 
# switch to image directory
cd $(dirname $0) 

case "$1" in

session-setup)
cp -a sdcard.img session-sdcard.img
cp -a flash.bin session-flash.bin
;;

session)
exec qemu-system-arm  \
    -machine virt \
    -cpu cortex-a15 \
    -smp 1 \
    -m 1024 \
    -pflash session-flash.bin \
    -nographic \
    -drive if=none,file=session-sdcard.img,id=hd0  \
    -device virtio-blk-device,drive=hd0  \
    -serial mon:stdio \
    -dtb armvirt.dtb
    ;;

session-teardown)
rm -f session-*.bin session-*.img
;;

*)
exec qemu-system-arm  \
    -machine virt \
    -cpu cortex-a15 \
    -smp 1 \
    -m 1024 \
    -pflash flash.bin \
    -nographic \
    -drive if=none,file=sdcard.img,id=hd0  \
    -device virtio-blk-device,drive=hd0 \
    -serial mon:stdio \
    -dtb armvirt.dtb
;;
esac
